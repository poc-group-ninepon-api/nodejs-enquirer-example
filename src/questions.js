function languageChoice(){
    return { 
        type: 'select',
        message: 'Please select language...',
        name: "language",
        choices: ['Thai', 'English'],
        validate: function(answer) {
            return true;
        },
        clearPromptOnDone: true,
    }
}

function mainMenuChoice(){
    return { 
        type: 'select',
        message: 'Please select menu...',
        name: "mainMenu",
        choices: [
            '⛅ Change environment', 
            '🤖 Update OS', 
            '💻 Teamviewer ID',
            // '🌍 Change language', 
            '🚪 Exit'
        ],
        clearPromptOnDone: true,
    }
}

function environmentChoice(){
    return { 
        type: 'select',
        message: 'Please select environment...',
        name: "environment",
        choices: [
            '🌕 Develop', 
            '🌞 Production',
            '🚪 Back previously'
        ],
        clearPromptOnDone: true,
    }
}

module.exports = {
    languageChoice,
    mainMenuChoice,
    environmentChoice
}
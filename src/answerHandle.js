const { exec, spawn } = require('node:child_process');
const axios = require('axios');

function _handleError(){
    console.log(`❌ something went wrong`);
}

async function getTeamviewerID(){
    console.clear();
    const { stdout, stderr } = await exec('getTeamviewerID');
    if(stderr){
        _handleError()
    }else{
        console.log(`✅ TeamviewerID: ${stdout.trim()}`);
    }
}

async function getAllVersion(){
    console.clear();
    const { stdout, stderr } = await exec('get-version');
    if(stderr){
        _handleError()
    }else{
        console.log(stdout);
    }
}

async function _updateWithRepository(state){
    switch(state.repository){
        case "all":
            exec('all-update');
        break;
        case "api":
            exec('api-update');
        break;
        case "frontend":
            console.log(`9999999`);
            exec('bash test.sh', (error, stdout, stderr) =>{
                if (error) {
                    console.error(`exec error: ${error}`);
                    return;
                }
                console.log(`stdout: ${stdout}`);
                console.error(`stderr: ${stderr}`);
            });
        break;
        case "so":

        break;
        case "ota":
            exec('ota-update');
        break;
    }
}

async function _changeENVWithRepository(state){
    const env = state.environment
    switch(state.repository){
        case "all":
            exec("all-env " + `"${env}"`);
        break;
        case "api":
            exec('api-env' + `"${env}"`);
        break;
        case "frontend":
            exec('frontend-env' + `"${env}"`);
        break;
        case "so":
            exec('so-env' + `"${env}"`);
        break;
        case "ota":
            exec('ota-update' + `"${env}"`);
        break;
    }
}

async function handleRepository(state){
    console.clear();
    switch(state.actionWithRepository){
        case "updateTagsVersion":
            _updateWithRepository(state)
        break;
        case "changeEnvironment":
            _changeENVWithRepository(state)
        break;
    }

    const { stdout, stderr } = await exec('get-version');
    if(stderr){
        _handleError()
    }else{
        console.log(stdout);
    }
}

async function getCurrentState(){
    try{
        const { data } = await axios.get("http://127.0.0.1:3333/state")
        console.log(data);
        // test()
    }catch(err){
        if(err.code === 'ECONNREFUSED'){
            console.log("PM2 hasn't start...");
        }else{
            console.log(err.response.status);
            console.log(err.response.statusText);
        }
        
    }
}

async function test(){
    console.log(`------ BLOCK ------`);
    
    const child = spawn('watch', ['-n', '2', 'ls']);
    child.stdout.on('data', (data) => {
        console.log(`stdout: ${data}`);
    });
    
    child.stderr.on('data', (data) => {
        console.error(`stderr: ${data}`);
    });
    
    child.on('close', (code) => {
        console.log(`child process exited with code ${code}`);
    });

    console.log(`------ BLOCK ------`);
}

module.exports = {
    getAllVersion,
    getTeamviewerID,
    handleRepository,
    handleRepository,
    getCurrentState
}
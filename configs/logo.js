const configLogo = {
    saveGradientColor: true,
    manyColorsOfGradient: 3,
    loadLogoFromLib: {
        active: true,
        options: {
            font: "slant",
            horizontalLayout: 'default', //"default", "full", "fitted", "controlled smushing", and "universal smushing"
            verticalLayout: 'default',
            width: 100,
            whitespaceBreak: true
        }
    }
}

module.exports = configLogo
const { prompt } = require('enquirer');
const { generateViaLibs } = require("./assets/logo.js")
const configLogo = require("./configs/logo.js")
const { gradientColors, bucketColors } = require("./helpers/gradient-colors.js")
const {
    languageChoice,
    mainMenuChoice,
    environmentChoice,
} = require("./src/questions.js")

const stateStore = {
    logo: "",
    language: "English",
    environment: "",
    repository: "",
    actionWithRepository: "",
    cuurentMenu: "",
    previuosMenu: "",
    gradientColorsLogo: [],
}
const prefixWords = {
    describeRepositoryMenu: "",
}

async function start(){
    // getCurrentState()
    await init()
    mainMenu()
}

function  _logoIcon(){
    return "LOGO" //
}

function  _getLogo(){
    if(configLogo.saveGradientColor){
        if(stateStore.gradientColorsLogo.length <= 0){
            stateStore.gradientColorsLogo = bucketColors()
        }
    }
    console.log(
        gradientColors(stateStore.gradientColorsLogo)
        (`${ stateStore.logo }`)
    );
}

function clearAndLogo(){
    console.clear();
    _getLogo();
    if(prefixWords.describeRepositoryMenu !== ""){
        console.log(`${prefixWords.describeRepositoryMenu}`);
    }
}

async function init(){
    console.log(configLogo.loadLogoFromLib.active);
    if(configLogo.loadLogoFromLib.active){
        stateStore.logo = await generateViaLibs()
    }else{
        stateStore.logo = _logoIcon()
    }
}

function mainMenu(){
    clearAndLogo()
    prompt( mainMenuChoice )
        .then( async (answers) => {
            stateStore.previuosMenu = 'mainMenu'
            switch(answers.mainMenu){
                case "⛅ Change environment":
                    stateStore.actionWithRepository = "changeEnvironment"
                    prefixWords.describeRepositoryMenu = `⛅ Change environment`
                    environmentMenu()
                break;
                case "🤖 Update OS":
                    stateStore.actionWithRepository = "updateTagsVersion"
                    prefixWords.describeRepositoryMenu = `🤖 Update version on repository...`
                    repositoryMenu()
                break;
                case "💻 Teamviewer ID":
                    prefixWords.describeRepositoryMenu = `💻 Teamviewer ID`
                    getTeamviewerID();
                break;
                case "🚪 Exit":
                    console.clear();
                break;
            }
        })
        .catch((error)=> console.log('Error exit..:', error))
}

start()
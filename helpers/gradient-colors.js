const gradient = require('gradient-string');
const configLogo = require("../configs/logo.js")

const _colors = [
    'red',
    'orange',
    'yellow',
    'green',
    'teal',
    'blue',
    'purple',
    'pink',
    'brown',
    'grey',
    'black',
    'white'
]

function _randomColors(round){
    const bucketColors = []
    for(let i = 0 ; i < round ; i++){
        let randomNumber = null
        do{
            randomNumber = Math.floor(Math.random() * _colors.length);
        } while (randomNumber === null || bucketColors.indexOf(_colors[randomNumber]) > -1 )
        bucketColors.push(_colors[randomNumber])
    }
    return bucketColors
}

function gradientColors(useArrayColor){
    let bucketColors = useArrayColor
    if(useArrayColor.length <= 0){
        bucketColors = _randomColors(configLogo.manyColorsOfGradient)
    }
    return gradient(bucketColors)
}

function bucketColors(){
    return _randomColors(configLogo.manyColorsOfGradient)
}

module.exports = {
    gradientColors,
    bucketColors
}
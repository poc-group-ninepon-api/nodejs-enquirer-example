const configApp = require("../configs/app.js")
const configLogo = require("../configs/logo.js")
var figlet = require('figlet');

function generateViaLibs(){
    return new Promise((resolve, reject) => {
        figlet(configApp.appName, configLogo.loadLogoFromLib.options, (err, data) => {
            if (err) {
                // console.log('Something went wrong...');
                // return reject(err);
                return resolve("=== LOAD LOGO HAS ERROR ===");
            }
            return resolve(data);
        });
    })
}

module.exports = {
    generateViaLibs,
}